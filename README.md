# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?(2016)
```plantuml
@startmindmap
+ programando en ordenadores 80's y actualidad
++ 80's
+++ Recursos muy limitados
+++ Lenguaje de programación
++++ Lenguaje de bajo nivel
+++++ Lenguaje ensamblador
++++++ Desventajas
+++++++ Era muy dificil programar en este lenguaje, debido a que se requeria mucho conocimiento del hardware
+++++++ Si algo salía mal era tu culpa
++++++ Ventajas
+++++++ Era rápido de ejecutar (para su época)
++ Actualidad
+++ Computadoras más potentes
+++ Lenguaje de programación
++++ Lenguaje de alto nivel
+++++ Más fáciles de programar y entender
++++++ Ejemplos
+++++++ c++
+++++++ C#
+++++++ Java
+++++ Desventajas
++++++ Son más lentas a la hora de ejecutar
+++++ Ventajas
++++++ No es necesario tener conocimientos del hardware
@endmindmap
```
